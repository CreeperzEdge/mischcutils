package faith.mom_gay.mischcutils;

import com.earth2me.essentials.Essentials;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

public class ConnectionListener implements Listener {

    private Main main;
    public ConnectionListener(Main main) {
        this.main = main;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        UUID pID = p.getUniqueId();
        Date date = new Date();
        long t = date.getTime();
        File userdata = new File(main.getDataFolder(), File.separator + "playerdata");
        File f = new File(userdata, File.separator + pID + ".yml");
        FileConfiguration playerData = YamlConfiguration.loadConfiguration(f);
        Essentials ess = (Essentials) event.getPlayer().getServer().getPluginManager().getPlugin("Essentials");
        if (!f.exists()) {
            try {
                playerData.createSection("playtime");
                playerData.createSection("startTime");
                playerData.createSection("dead");
                playerData.createSection("god");
                playerData.set("playtime", "0");
                playerData.set("startTime", t);
                playerData.set("dead", false);
                playerData.set("god", true);
                playerData.save(f);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } else {
            try {
                playerData.set("startTime", t);
                if (playerData.getBoolean("god")) {
                    ess.getUser(p).setGodModeEnabled(true);
                }
                playerData.save(f);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) throws FileNotFoundException {
        Player p = event.getPlayer();
        UUID pID = p.getUniqueId();
        Date date = new Date();
        long t = date.getTime();
        File userdata = new File(main.getDataFolder(), File.separator + "playerdata");
        File f = new File(userdata, File.separator + pID + ".yml");
        FileConfiguration playerData = YamlConfiguration.loadConfiguration(f);
        if (!f.exists()) {
            throw new FileNotFoundException("data file not found");
        } else {
            try {
                long start = playerData.getLong("startTime");
                long time = t - start;
                playerData.set("playtime", playerData.getLong("playtime") + time);
                playerData.save(f);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) throws FileNotFoundException{
        Player p = event.getEntity();
        p.getWorld().strikeLightningEffect(p.getLocation());
        if (p.getServer().isHardcore()) {
            UUID pID = p.getUniqueId();
            Date date = new Date();
            long t = date.getTime();
            File userdata = new File(main.getDataFolder(), File.separator + "playerdata");
            File f = new File(userdata, File.separator + pID + ".yml");
            FileConfiguration playerData = YamlConfiguration.loadConfiguration(f);
            if (!f.exists()) {
                throw new FileNotFoundException("data file not found");
            } else {
                try {
                    long start = playerData.getLong("startTime");
                    long time = t - start;
                    playerData.createSection("deathX");
                    playerData.createSection("deathY");
                    playerData.createSection("deathZ");
                    playerData.set("playtime", playerData.getLong("playtime") + time);
                    playerData.set("dead", true);
                    playerData.set("deathX", p.getLocation().getBlockX());
                    playerData.set("deathY", p.getLocation().getBlockY());
                    playerData.set("deathZ", p.getLocation().getBlockZ());
                    playerData.save(f);
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }
}
