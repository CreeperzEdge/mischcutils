package faith.mom_gay.mischcutils;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class CommandWild implements CommandExecutor {

    private Main main;
    public CommandWild(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Block origloc = ((Player) sender).getLocation().getBlock();
            sender.sendMessage("Teleportation will commence in 15 seconds. Don't move.");
            BukkitTask task = new BukkitRunnable() {
                @Override
                public void run() {
                    sender.sendMessage("Teleporting!");
                    sender.getServer().dispatchCommand(sender.getServer().getConsoleSender(), "spreadplayers 0 0 500 2500 false " + ((Player) sender).getName());
                    this.cancel();
                }
            }.runTaskLater(main, 15*20);

            BukkitTask check = new BukkitRunnable() {
                @Override
                public void run() {
                    System.out.println(task.isCancelled());
                    if (!((Player) sender).getLocation().getBlock().equals(origloc) && !task.isCancelled()) {
                        sender.sendMessage("Teleportation cancelled.");
                        task.cancel();
                        this.cancel();
                    } else if (task.isCancelled()) {
                        this.cancel();
                    }
                }
            }.runTaskTimer(main, 0, 20);
            return true;
        } else return false;
    }
}
