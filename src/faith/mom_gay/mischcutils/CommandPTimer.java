package faith.mom_gay.mischcutils;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.Date;
import java.util.UUID;

public class CommandPTimer implements CommandExecutor {

    private Main main;
    public CommandPTimer(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.getServer().broadcastMessage("Starting!");
        File dir = new File(main.getDataFolder() + File.separator + "playerdata");
        File[] directoryListing = dir.listFiles();
        Date date = new Date();
        long t = date.getTime();
        sender.getServer().getWorld("world").setPVP(false);
        if (directoryListing != null) {
            for (File child : directoryListing) {
                UUID pID = UUID.fromString(child.getName().substring(0, child.getName().length() - 4));
                FileConfiguration playerData = YamlConfiguration.loadConfiguration(child);
                playerData.set("dead", false);
                playerData.set("playtime", 0L);
                playerData.set("god", true);
                if (sender.getServer().getOfflinePlayer(pID).isOnline()) {
                    playerData.set("startTime", t);
                }
            }
            sender.getServer().dispatchCommand(sender.getServer().getConsoleSender(), "spreadplayers 0 0 500 2500 false @a");
            sender.getServer().dispatchCommand(sender.getServer().getConsoleSender(), "deop @a");
            sender.getServer().dispatchCommand(sender.getServer().getConsoleSender(), "ci *");
            BukkitTask task = new BukkitRunnable() {
                @Override
                public void run() {
                    sender.getServer().broadcastMessage("PVP has started!");
                    for (Player pl : sender.getServer().getOnlinePlayers()) {
                        pl.playSound(pl.getLocation(), Sound.BLOCK_ANVIL_DESTROY, 10f, 1f);
                    }
                    sender.getServer().getWorld("world").setPVP(true);
                }
            }.runTaskLater(main, 20*7200);
        }
        return true;
    }
}
