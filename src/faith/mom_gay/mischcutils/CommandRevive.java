package faith.mom_gay.mischcutils;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.UUID;

public class CommandRevive implements CommandExecutor {

    private Main main;
    public CommandRevive(Main main) {
        this.main = main;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 2) {
            return false;
        } else {
            if (sender.getServer().getOfflinePlayer(args[0]).isOnline()) {
                sender.getServer().dispatchCommand(sender.getServer().getConsoleSender(), "minecraft:gamemode survival " + args[0]);
                if (Boolean.parseBoolean(args[1])) {
                    UUID pID = sender.getServer().getPlayer(args[0]).getUniqueId();
                    File userdata = new File(main.getDataFolder(), File.separator + "playerdata");
                    File f = new File(userdata, File.separator + pID + ".yml");
                    FileConfiguration playerData = YamlConfiguration.loadConfiguration(f);
                    Location death = new Location (sender.getServer().getPlayer(pID).getWorld(), playerData.getInt("deathX"), playerData.getInt("deathY"), playerData.getInt("deathZ"));
                    sender.getServer().getPlayer(pID).teleport(death);
                }
                sender.sendMessage("Revived " + args[0]);
                return true;
            } else {
                return false;
            }
        }
    }

}
