package faith.mom_gay.mischcutils;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.UnsupportedEncodingException;

public class BrandListener implements PluginMessageListener {
    @Override
    public void onPluginMessageReceived(String channel, Player p, byte[] msg) {
        try {
            p.getServer().broadcastMessage(p.getDisplayName() + " connected with 1.13.2/" + new String(msg, "UTF-8").substring(1));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
